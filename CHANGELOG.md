# CHANGELOG

## v0.7.1 (2022-11-11)

### Fix

- delete all .egg-info when running make clean-build

## v0.7.0 (2022-11-11)

### Feat

- add a config module in tools to read config files in config_files

## v0.6.0 (2022-11-04)

### Feat

- add tox support and predifined jobs for tests, lint and mypy

## v0.5.0 (2022-10-30)

### Feat

- add a makefile to generate doc and clean project

## v0.4.0 (2022-10-30)

### Feat

- add Sphinx template for easy documention startup

## v0.3.0 (2022-10-28)

### Feat

- Add minimal install step for the python project in README.md

## v0.2.1 (2022-10-28)

### Fix

- fix wrong tags in pyproject.toml and readme that were not replaced correctly

## v0.2.0 (2022-10-26)

### Feat

- add minimal python packaging support through setuptools

## v0.1.1 (2022-10-25)

### Refactor

- move current logic to installer as was done in Basic 13.2

## v0.1.0 (2022-10-15)

### Feat

- Add a first usable version based mainly on Basic

## v0.0.0 (2022-10-07)
