#!/bin/sh
# Author : Frédéric Bègue

########################
# Variables definition #
########################

is_python3="true"

project_dir=$(dirname "$0")
cd "$project_dir" || exit
project_dir=$(pwd)

# shellcheck source-path=SCRIPTDIR/
main_dir="$project_dir/src/git-submodules/installer/"

#######################
# Updating submodules #
#######################

git submodule update --init --recursive

#####################
# Get configuration #
#####################

# shellcheck source-path=SCRIPTDIR/
. "$project_dir/config"

####################
# Project Creation #
####################

# shellcheck source-path=SCRIPTDIR/
. "$project_dir/src/git-submodules/installer/main.sh"