# Configuration file

## About

When running the init.sh file *as is*,  the program will asked for various inputs in order to configure your project.  
You might skip those prompts by giving directly the values in the *config* file at the root of this repository.

If a variable is not set or set to an empty string, you will be prompted to input a value for it after running init.sh.
If a variable is set to a non empty string which is not valid, you will be prompted to input a valid value for it after running init.sh.
If a variable is set to a valid non empty string, you won't be prompted to input a value for it after running init.sh.

## List of available variables (and their use)

- project_name
    - The name of your project
    - Used to personalize your README.md file and to name your remote gitlab project if needed
    - Should not contain any '/' ('/' will be removed silently)
    - If you want Gitlab Projects Templates to create your remote Gitlab server, it should comply with Gitlab project naming policy
    - If you want Gitlab Projects Templates to create your remote Gitlab server, it should not be already used by another project in the namespace
- project_description
    - A short description of your project (use '\n' for multiline input)
    - Used to personalize your README.md file and the description of your remote Gitlab project if needed
    - Should not contain any '/' ('/' will be removed silently)
- project_authors
    - The author(s) of your projects (use ',' as a separator)
    - Used to personalize your README.md
    - Should not contain any '/' ('/' will be removed silently)
- project_version
    - The starting version of your project in the form X.Y.Z
    - Defaults to *0.0.0*
    - Used to initialize the version of your project in VERSION and .cz.toml and to create the first tag
- git_user_name
    - Username to set in your local git config file (project config)
    - Will be used for the initial commit
- git_ser_mail
    - Mail to set in your local git config file (project config)
    - Will be used for the initial commit
- gitlab_server
    - URL of your remote gitlab repository
    - Defaults to *gitlab.com*
- gitlab_ssh_host
    - Host name to use for the ssh connection (useful if you have multiple account on the same gitlab server and manage them through ssh config file)
    - Defaults to your inputted value of *gitlab_server*
- gitlab_token
    - A valid Personal Gitlab Access Token
    - Used to authenticate through gitlab API
- gitlab_namespace
    - An **existing** valid gitlab namespace
    - The project will be created in that namespace
    - The user linked to the gitlab token should have enough rights to create a project in that namespace
- gitlab_slug
    - The Gitlab project slug (will be used in the url path to the project)
    - Should comply with Gitlab project slug naming policy
    - Should not be already used by another project in the namespace
- python_project_name
    - Name to be used as project name in Pypi
    - Should use only ASCII alphanumerics characters and the following special character '-', '_' or '.'
    - Should not start with, end with or have consecutive special characters
- python3_minimum_required_minor
    - The minimum python version to be used with your project
    - Should be an integer number
