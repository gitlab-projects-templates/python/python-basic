# Introduction

## About Gitlab Projects Templates - Python-Basic

Gitlab Projects Templates aims to provide a few templates for starting a new project faster on Gitlab.

Python-Basic is the simplest of those templates designed for python projects and could be used for virtually any kind of python project for which a specific template does'nt yet exist.

It is designed to run with minimal requirements (a posix shell, git, jq, ssh_keygen and of course python) and with no assumptions on what type of project will be created.

Gitlab Projects Templates - Python-Basic creates a new project into the directory where the template is pulled and run. So, to avoid loosing data, you should create a new directory in which you pull this project. You then just have to run the init.sh file and follow what's prompted ([see README.md](../README.md#getting-started)).

That's all you need to do to get started!

[back to top](#introduction)

## What it does

When running the init.sh file *as is*, the program will first check the requirements and then ask for various information in order to configure your project.  
You might skip those prompts by giving directly the values in the *config* file at the root of this repository (see [config file documentation](./config.md) for more information)

After that, the starting files of your project are created. This include :

- the README.md, VERSION, LICENSE, CHANGELOG.md and .gitignore files and the src, docs, and tests directories
- a working gitlab-ci pipeline (see [gitlab pipeline documentation](./gitlab-ci%20pipeline.md) for more information) containing :
    - placeholders for build, test and deploy jobs
    - quality code check (fixme, git-legal, shellcheck and markdownlint)
    - SAST check
    - Auto bump feature

Then the git repository is created locally. The git information (username and mail) will be set for this project in the project config file and a first commit will be made, named "initial commit".

At this point the program will ask you if you want to setup the remote gitlab server. You can skip this question and automatically answer yes or no by adding the -y or -n option respectively when running the init.sh script.

If you say *yes*, the program will ask you for more information in order to configure your remote server and then :

- create the remote repository and configure it (private repository with fast forward merges only, squash commit required, merge only if all discussions resolved, pipelines succeeds and are not skipped, remove merged branches enabled and auto-devops disabled)
- create an ssh key pair, use the public part to create a deploy key and the private part to create a CI variable to be able to push during a CI job.
- push the initial commit to the remote server

Finally, some cleaning is done, including removing useless files for your project (including this documentation)

[back to top](#introduction)

## License

Gitlab Projects Templates - Python-Basic is published under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

Note that this does not restrict the type of license for your new project created with this template. It only restrict the type of license you may use when **distributing** modified or unmodified copy of this template.

[back to top](#introduction)
