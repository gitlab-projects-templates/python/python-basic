# Gitlab CI pipeline

## About

Gitlab Projects Templates - Python-Basic provides a working gitlab-ci pipeline containing :

- placeholders for build, test and deploy jobs
- quality code checks (fixme, git-legal, shellcheck and markdownlint)
- SAST checks
- Auto bump feature

Here are some explanation about this.

### Placeholders for build, test and deploy jobs

The .gitlab-ci.yml file contains dummy jobs named build-job, test-job and deploy-prod that can be used as placeholders in the early steps of your project, before the CI becomes more advanced.

### Quality code checks

The CI pipeline includes the [Code Quality template](https://docs.gitlab.com/ee/ci/testing/code_quality.html)) from Gitlab.  
It is configure within the .codeclimate.yml file at the root of the repository. This file might be modified to suit your needs.  
It includes :

- **FIXME**: search for the following strings in your project: TODO, FIXME, HACK, XXX, and BUG, which are all things you should fix now, not later.
- **git-legal**: scans the libraries used by your project and flags potential compliance and compatibility issues
- **markdownlint**: a tool designed to check markdown files and flag style issues (configured in .mdlrc and .mdlrc.style.rb files)
- **ShellCheck** : a tool for suggesting possible improvements to bash/sh shell scripts.

Code quality might be disabled by setting a non empty value to the **CODE_QUALITY_DISABLED** at the top of the .gitlab-ci.yml file.

### SAST Checks

The CI pipeline includes the [SAST template](https://docs.gitlab.com/ee/user/application_security/sast/)) and the [Secret detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/) from Gitlab.

Those checks might be disabled by setting a non empty value to the **SAST_DISABLED** and **SECRET_DETECTION_DISABLED** variables at the top of the .gitlab-ci.yml file.

### Auto bump

The .gitlab-ci.yml provide a bump-version job that automatically bump the version (and create a new commit and tag with the bumped version) when a push is made on the default branch.

The auto-bump is made with [commitizen](https://commitizen-tools.github.io/commitizen/) which is configured in the .cz.toml file.

You can disable the auto-bump by setting a non empty value for the AUTOBUMP_DISABLED in .gitlab-ci.yml after running ./init.sh or by adding any of \[nobump\], \[no bump\], \[no-bump\], \[skip bump\], \[skip-bump\], \[bump skip\] or \[bump-skip\] in the commit message)

#### Commit message

For the Autobump feature to work, commit message must respect [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) on the main branch.

To help enforce this, we :

- set the default squash commit message in merge request to :
    - first line : title of the merge request
    - description : description of the merge request
- control in a job named *check_merge_request_title* that the title of the merge request is a well formed conventional commit message. This job might be disabled by setting a non empty value to the **MR_TITLE_CHECK_DISABLED** variable at the top of the .gitlab-ci.yml file.

#### Auto bump docker image

The auto-bump job use a docker image named fkbbegue/auto-bump that contains :

- commitizen
- the ssh host keys of gitlab.com hardcoded

The code for creating this docker image is available in gitlab, in the same group as this project (Gitlab Projects Templates) [Autobump Dockerfiles](https://gitlab.com/gitlab-projects-templates/tools/autobump-dockerfiles)

#### Use of a self-hosted gitlab server

If your repository is on gitlab.com, the ssh host keys of gitlab.com that are hardcoded in the docker image allow a secure communication between the container in which the auto-bump job is run and gitlab.com during the push on your repository.

If you use a self-hosted gitlab-server, the auto bump should fail because your gitlab host key will not be recognize. To resolve this problem you might either :

- uncomment the ``ssh-keyscan ${CI_SERVER_HOST} >> ~/.ssh/known_hosts`` in the gitlab-ci configuration file
- produce your own docker image with your own ssh host key and correspondingly replace the images in the gitlab-ci configuration file. (Use [Autobump Dockerfiles](https://gitlab.com/gitlab-projects-templates/tools/autobump-dockerfiles))
- add the host keys of your server to *~/.ssh/known_hosts* in the gitlab-ci configuration file at any point before executing the ``git push origin HEAD:$CI_COMMIT_REF_NAME`` line.

The gitlab-ci configuration file to modify should be :

- init_files/init_.gitlab-ci.yml if you do it before running init.sh
- .gitlab-ci.yml if you do it after running init.sh  

NB: the first solution is the simplest one but also the less safe one as it might be vulnerable to man in the middle attacks. But the risk is mostly mitigated if your network between your gitlab server and your gitlab runners is safe.

#### Deploy key

For the auto-bump job to work, a deploy key is created (named **CI Access key** and belonging to the user associated with the gitlab token used during project creation) and push permission are given to the *Maintainer* role.  
If you want to forbid any user to push to the default branch or if you want to remove push rights to the project creator, you will need either :

- to allow this deploy key (named **CI Access key**) to push for the autobump feature to work (in project Settings/Repository/Protected_branches in the Gitlab UI)
- To create a new deploy key with write permission (in project Settings/Repository/Deploy keys in the Gitlab UI), give it push rights (in project Settings/Repository/Protected_branches in the Gitlab UI) AND change the value of the CI variable SSH_PRIVATE_KEY to the corresponding private ssh key of your naw deploy key.
