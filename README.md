# Python-Basic

[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-%23FE5196?logo=conventionalcommits&logoColor=white)](https://www.conventionalcommits.org)
[![img](https://img.shields.io/badge/semver-2.0.0-green)](https://semver.org/)

Gitlab Projects Templates aims to provide a few templates for starting a new project faster on Gitlab.

Python-Basic is the simplest of the Python templates and could be used for virtually any python project for which a specific template doesn't exist yet.

## Getting Started

### Requirements

- Python 3.8+
- pip 21.3+
- Git 2.28+
- jq 1.6+ (used to treat json answers of the gitlab API)
- ssh-keygen
- a Gitlab account (on gitlab.com or on a self-hosted gitlab instance 14.6+) and an associated gitlab access token [(see here)](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) in order to create the gitlab repository

[back to top](#python-basic)

### Installation

To install this template, you may simply clone this project, modify the config file if needed (see [configuration file documentation](/docs/config.md) for more information), and then run the init.sh

1. clone the project

    ```sh
    git clone https://gitlab.com/gitlab-projects-templates/python/python-basic.git my-awesome-python-project
    cd my-awesome-python-project
    ```

1. modify the config file (optional - see ./docs for information on the use of the configuration file and/or type ``./init.sh -h``)

1. run the init.sh file

- On Linux / macOS

    ```sh
    ./init.sh
    ```

- On Windows, you will need a POSIX shell. This shouldn't be a problem if you have WSL enabled or if you have installed git with [Git for Windows](https://gitforwindows.org/) ) and then run :

    ```sh
    bash init.sh
    ```

### Gitlab CI configuration for autobump

The .gitlab-ci.yml provide a bump-version job that automatically bump the version (and create a new commit and tag with the bumped version) when a push is made on the default branch (see [gitlab ci documentation](/docs/gitlab-ci%20pipeline.md#auto-bump) for more information.)

The autobump job needs a working ssh connection between your Gitlab Runner where the continuous integration is executed and your Gitlab repository. This will need some more configuration if you have a self-hosted gitlab server. See [gitlab ci documentation](/docs/gitlab-ci%20pipeline.md#use-of-a-self-hosted-gitlab-server) for more information.

(NB : You can disable the auto-bump by setting a non empty value for the AUTOBUMP_DISABLED variable in .gitlab-ci.yml after running ./init.sh or by adding any of \[nobump\], \[no bump\], \[no-bump\], \[skip bump\], \[skip-bump\], \[bump skip\] or \[bump-skip\] in the commit message)

[back to top](#python-basic)

## Choices and Beliefs

From our point of view, the goal of a good template is to lessen the endless discussions at the start of a project (like which linter or which commit convention to use...).

To achieve this in this template we sometimes relied on strong beliefs we have. And sometimes we had to make choices.

Here is a summary of those choices and beliefs

### General standards

- We believe that there exist a good tool for project templating named [cookiecutter](https://github.com/cookiecutter/cookiecutter)
- We choose not to use it in the early version as Python-Basic is built on Basic, but we will most certainly use it in later version
- We believe that continuous integration (CI) is almost always a better idea
- We choose Gitlab-CI as a CI tool

### Git standards

- We choose Gitlab as a repository management platform
- We believe that SemVer 2.0.0 and Conventional Commit 1.0.0 should be followed whenever possible
- We chose commitizen-tools to help enforce their use through the CI
- We believe that a linear history in git make the project more readable, especially when the details of the development process are available in the merge request history.
- We choose to enforce fast forward only merge on the default branch with squash commit.
- We believe that no one should be able to push to the default branch. This should be the job of the CI. But in the beginning of a new project, this rule could be loosen.
- We choose to allow Maintainers to push by default when creating the project. This should be changed when needed.

### Code Quality

- We believe that code quality issues are important but shouldn't blindly block release
- We believe that running quality checks on every commit might be a loss of time as this can make CI pipeline considerably longer than necessary (especially at the beginning of a project)
- We believe that code quality is a responsibility shared between the developer who writes the feature and the person who merges it.
- We thus choose to:
    - provide by default a minimal set of tools for quality checks (fixme, git-legal, shellcheck and markdownlint) and security check (SAST and secret detection) for general purpose projects
    - not include code quality checks in branch pipelines : the developer should have locally all the tools he needs to write code conform to the team's standard
    - include code quality checks in merge request pipeline on manual mode : it's thus the responsibility of the approver to run it at least once before merging

[back to top](#python-basic)

## Changelog

See the CHANGELOG.md file

[back to top](#python-basic)

## License

Gitlab Projects Templates - Python-Basic  
Copyright (C) 2022  Frédéric Bègue  
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

[back to top](#python-basic)
