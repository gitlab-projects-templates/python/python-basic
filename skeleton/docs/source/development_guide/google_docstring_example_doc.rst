Google Docstrings in api-docs sample
====================================

.. raw:: html

  <section id="module-awesome_project.exemple">
  <span id="awesome-project-exemple-module"></span><h1>awesome_project.exemple module<a class="headerlink" href="#module-awesome_project.exemple" title="Permalink to this heading"></a></h1>
  <p>Example Google style docstrings.</p>
  <p>This code demonstrates documentation as specified by the Google Python
  Style Guide. Docstrings may extend over multiple lines. Sections are created
  with a section header and a colon followed by a block of indented text.</p>
  <div class="admonition-example admonition">
  <p class="admonition-title">Example</p>
  <p>Examples can be given using either the <code class="docutils literal notranslate"><span class="pre">Example</span></code> or <code class="docutils literal notranslate"><span class="pre">Examples</span></code>
  sections. Sections support any reStructuredText formatting, including
  literal blocks:</p>
  <div class="highlight-default notranslate"><div class="highlight"><pre><span></span>$ python example_google.py
  </pre></div>
  </div>
  </div>
  <p>Section breaks are created by resuming unindented text. Section breaks
  are also implicitly created anytime a new section starts.</p>
  <dl class="py data">
  <dt class="sig sig-object py" id="awesome_project.exemple.module_level_variable">
  <span class="sig-prename descclassname"><span class="pre">awesome_project.exemple.</span></span><span class="sig-name descname"><span class="pre">module_level_variable</span></span><em class="property"><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="pre">int</span></em><em class="property"><span class="w"> </span><span class="p"><span class="pre">=</span></span><span class="w"> </span><span class="pre">12345</span></em><a class="headerlink" href="#awesome_project.exemple.module_level_variable" title="Permalink to this definition"></a></dt>
  <dd><p>Module level variable are documented inline.</p>
  <p>The docstring may span multiple lines. The type is hinted.</p>
  </dd></dl>

  <dl class="py function">
  <dt class="sig sig-object py" id="awesome_project.exemple.module_level_function">
  <span class="sig-prename descclassname"><span class="pre">awesome_project.exemple.</span></span><span class="sig-name descname"><span class="pre">module_level_function</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">param1</span></span><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="n"><span class="pre">int</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">param2</span></span><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="n"><span class="pre">Optional</span><span class="p"><span class="pre">[</span></span><span class="pre">str</span><span class="p"><span class="pre">]</span></span></span><span class="w"> </span><span class="o"><span class="pre">=</span></span><span class="w"> </span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="o"><span class="pre">*</span></span><span class="n"><span class="pre">args</span></span></em>, <em class="sig-param"><span class="o"><span class="pre">**</span></span><span class="n"><span class="pre">kwargs</span></span></em><span class="sig-paren">)</span><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#awesome_project.exemple.module_level_function" title="Permalink to this definition"></a></dt>
  <dd><p>This is an example of a module level function.</p>
  <p>Function parameters should be documented in the <code class="docutils literal notranslate"><span class="pre">Args</span></code> section. The name
  of each parameter is required. The description of each parameter
  is optional, but should be included if not obvious.</p>
  <p>If *args or **kwargs are accepted,
  they should be listed as <code class="docutils literal notranslate"><span class="pre">*args</span></code> and <code class="docutils literal notranslate"><span class="pre">**kwargs</span></code>.</p>
  <p>The format for a parameter is:</p>
  <div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">name</span><span class="p">:</span> <span class="n">description</span>
      <span class="n">The</span> <span class="n">description</span> <span class="n">may</span> <span class="n">span</span> <span class="n">multiple</span> <span class="n">lines</span><span class="o">.</span> <span class="n">Following</span>
      <span class="n">lines</span> <span class="n">should</span> <span class="n">be</span> <span class="n">indented</span><span class="o">.</span> <span class="n">The</span> <span class="s2">&quot;(type)&quot;</span> <span class="ow">is</span> <span class="n">optional</span><span class="o">.</span>

      <span class="n">Multiple</span> <span class="n">paragraphs</span> <span class="n">are</span> <span class="n">supported</span> <span class="ow">in</span> <span class="n">parameter</span>
      <span class="n">descriptions</span><span class="o">.</span>
  </pre></div>
  </div>
  <dl class="field-list simple">
  <dt class="field-odd">Parameters</dt>
  <dd class="field-odd"><ul class="simple">
  <li><p><strong>param1</strong> (<em>int</em>) – The first parameter.</p></li>
  <li><p><strong>param2</strong> (<em>Optional</em><em>[</em><em>str</em><em>]</em>) – The second parameter. Defaults to None.
  Second line of description should be indented.</p></li>
  <li><p><strong>*args</strong> – Variable length argument list.</p></li>
  <li><p><strong>**kwargs</strong> – Arbitrary keyword arguments.</p></li>
  </ul>
  </dd>
  <dt class="field-even">Returns</dt>
  <dd class="field-even"><p><p>True if successful, False otherwise.</p>
  <p>The <code class="docutils literal notranslate"><span class="pre">Returns</span></code> section may span multiple lines and paragraphs.
  Following lines should be indented to match the first line.</p>
  <p>The <code class="docutils literal notranslate"><span class="pre">Returns</span></code> section supports any reStructuredText formatting,
  including literal blocks:</p>
  <div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="p">{</span>
      <span class="s1">&#39;param1&#39;</span><span class="p">:</span> <span class="n">param1</span><span class="p">,</span>
      <span class="s1">&#39;param2&#39;</span><span class="p">:</span> <span class="n">param2</span>
  <span class="p">}</span>
  </pre></div>
  </div>
  </p>
  </dd>
  <dt class="field-odd">Raises</dt>
  <dd class="field-odd"><ul class="simple">
  <li><p><strong>AttributeError</strong> – The <code class="docutils literal notranslate"><span class="pre">Raises</span></code> section is a list of all exceptions
      that are relevant to the interface.</p></li>
  <li><p><strong>ValueError</strong> – If <cite>param2</cite> is equal to <cite>param1</cite>.</p></li>
  </ul>
  </dd>
  </dl>
  </dd></dl>

  <dl class="py function">
  <dt class="sig sig-object py" id="awesome_project.exemple.example_generator">
  <span class="sig-prename descclassname"><span class="pre">awesome_project.exemple.</span></span><span class="sig-name descname"><span class="pre">example_generator</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">n</span></span><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="n"><span class="pre">int</span></span></em><span class="sig-paren">)</span><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#awesome_project.exemple.example_generator" title="Permalink to this definition"></a></dt>
  <dd><p>Generators have a <code class="docutils literal notranslate"><span class="pre">Yields</span></code> section instead of a <code class="docutils literal notranslate"><span class="pre">Returns</span></code> section.</p>
  <dl class="field-list simple">
  <dt class="field-odd">Parameters</dt>
  <dd class="field-odd"><p><strong>n</strong> (<em>int</em>) – The upper limit of the range to generate, from 0 to <cite>n</cite> - 1.</p>
  </dd>
  <dt class="field-even">Yields</dt>
  <dd class="field-even"><p><em>int</em> – The next number in the range of 0 to <cite>n</cite> - 1.</p>
  </dd>
  </dl>
  <div class="admonition-examples admonition">
  <p class="admonition-title">Examples</p>
  <p>Examples should be written in doctest format, and should illustrate how
  to use the function.</p>
  <div class="doctest highlight-default notranslate"><div class="highlight"><pre><span></span><span class="gp">&gt;&gt;&gt; </span><span class="nb">print</span><span class="p">([</span><span class="n">i</span> <span class="k">for</span> <span class="n">i</span> <span class="ow">in</span> <span class="n">example_generator</span><span class="p">(</span><span class="mi">4</span><span class="p">)])</span>
  <span class="go">[0, 1, 2, 3]</span>
  </pre></div>
  </div>
  </div>
  </dd></dl>

  <dl class="py exception">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleError">
  <em class="property"><span class="pre">exception</span><span class="w"> </span></em><span class="sig-prename descclassname"><span class="pre">awesome_project.exemple.</span></span><span class="sig-name descname"><span class="pre">ExampleError</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">msg</span></span><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="n"><span class="pre">str</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">code</span></span><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="n"><span class="pre">int</span></span></em><span class="sig-paren">)</span><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#awesome_project.exemple.ExampleError" title="Permalink to this definition"></a></dt>
  <dd><p>Bases: <code class="xref py py-class docutils literal notranslate"><span class="pre">Exception</span></code></p>
  <p>Exceptions are documented in the same way as classes.</p>
  <p>The __init__ method may be documented in either the class level
  docstring, or as a docstring on the __init__ method itself.</p>
  <p>Either form is acceptable, but the two should not be mixed. Choose one
  convention to document the __init__ method and be consistent with it.</p>
  <div class="admonition note">
  <p class="admonition-title">Note</p>
  <p>Do not include the <cite>self</cite> parameter in the <code class="docutils literal notranslate"><span class="pre">Args</span></code> section.</p>
  </div>
  <dl class="field-list simple">
  <dt class="field-odd">Parameters</dt>
  <dd class="field-odd"><ul class="simple">
  <li><p><strong>msg</strong> (<em>str</em>) – Human readable string describing the exception.</p></li>
  <li><p><strong>code</strong> (<em>int</em>) – Error code.</p></li>
  </ul>
  </dd>
  </dl>
  <dl class="py attribute">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleError.msg">
  <span class="sig-name descname"><span class="pre">msg</span></span><a class="headerlink" href="#awesome_project.exemple.ExampleError.msg" title="Permalink to this definition"></a></dt>
  <dd><p>Human readable string describing the exception.</p>
  <dl class="field-list simple">
  <dt class="field-odd">Type</dt>
  <dd class="field-odd"><p>str</p>
  </dd>
  </dl>
  </dd></dl>

  <dl class="py attribute">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleError.code">
  <span class="sig-name descname"><span class="pre">code</span></span><a class="headerlink" href="#awesome_project.exemple.ExampleError.code" title="Permalink to this definition"></a></dt>
  <dd><p>Exception error code.</p>
  <dl class="field-list simple">
  <dt class="field-odd">Type</dt>
  <dd class="field-odd"><p>int</p>
  </dd>
  </dl>
  </dd></dl>

  </dd></dl>

  <dl class="py class">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleClass">
  <em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-prename descclassname"><span class="pre">awesome_project.exemple.</span></span><span class="sig-name descname"><span class="pre">ExampleClass</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">param1</span></span><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="n"><span class="pre">str</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">param2</span></span><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="n"><span class="pre">int</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">param3</span></span><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="n"><span class="pre">List</span><span class="p"><span class="pre">[</span></span><span class="pre">str</span><span class="p"><span class="pre">]</span></span></span></em><span class="sig-paren">)</span><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#awesome_project.exemple.ExampleClass" title="Permalink to this definition"></a></dt>
  <dd><p>Bases: <code class="xref py py-class docutils literal notranslate"><span class="pre">object</span></code></p>
  <p>The summary line for a class docstring should fit on one line.</p>
  <p>If the class has public attributes, they may be documented here
  in an <code class="docutils literal notranslate"><span class="pre">Attributes</span></code> section and follow the same formatting as a
  function’s <code class="docutils literal notranslate"><span class="pre">Args</span></code> section. Alternatively, attributes may be documented
  inline with the attribute’s declaration (see __init__ method below).</p>
  <p>Properties created with the <code class="docutils literal notranslate"><span class="pre">&#64;property</span></code> decorator should be documented
  in the property’s getter method.</p>
  <div class="admonition note">
  <p class="admonition-title">Note</p>
  <p>Do not include the <cite>self</cite> parameter in the <code class="docutils literal notranslate"><span class="pre">Args</span></code> section.</p>
  </div>
  <dl class="field-list simple">
  <dt class="field-odd">Parameters</dt>
  <dd class="field-odd"><ul class="simple">
  <li><p><strong>param1</strong> (<em>str</em>) – Description of <cite>param1</cite>.</p></li>
  <li><p><strong>param2</strong> (<em>int</em>) – Description of <cite>param2</cite>. Multiple
  lines are supported.</p></li>
  <li><p><strong>param3</strong> (<em>List</em><em>[</em><em>str</em><em>]</em>) – Description of <cite>param3</cite>.</p></li>
  </ul>
  </dd>
  </dl>
  <dl class="py attribute">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleClass.attr1">
  <span class="sig-name descname"><span class="pre">attr1</span></span><a class="headerlink" href="#awesome_project.exemple.ExampleClass.attr1" title="Permalink to this definition"></a></dt>
  <dd><p>Description of <cite>attr1</cite>.</p>
  <dl class="field-list simple">
  <dt class="field-odd">Type</dt>
  <dd class="field-odd"><p>str</p>
  </dd>
  </dl>
  </dd></dl>

  <dl class="py attribute">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleClass.attr2">
  <span class="sig-name descname"><span class="pre">attr2</span></span><a class="headerlink" href="#awesome_project.exemple.ExampleClass.attr2" title="Permalink to this definition"></a></dt>
  <dd><p>Description of <cite>attr2</cite>.</p>
  <dl class="field-list simple">
  <dt class="field-odd">Type</dt>
  <dd class="field-odd"><p>int</p>
  </dd>
  </dl>
  </dd></dl>

  <dl class="py attribute">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleClass.attr3">
  <span class="sig-name descname"><span class="pre">attr3</span></span><em class="property"><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="pre">List</span><span class="p"><span class="pre">[</span></span><span class="pre">str</span><span class="p"><span class="pre">]</span></span></em><a class="headerlink" href="#awesome_project.exemple.ExampleClass.attr3" title="Permalink to this definition"></a></dt>
  <dd><p>Comment <em>inline</em></p>
  </dd></dl>

  <dl class="py attribute">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleClass.attr4">
  <span class="sig-name descname"><span class="pre">attr4</span></span><em class="property"><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="pre">List</span><span class="p"><span class="pre">[</span></span><span class="pre">str</span><span class="p"><span class="pre">]</span></span></em><a class="headerlink" href="#awesome_project.exemple.ExampleClass.attr4" title="Permalink to this definition"></a></dt>
  <dd><p>Comment <em>before</em> attribute</p>
  </dd></dl>

  <dl class="py attribute">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleClass.attr5">
  <span class="sig-name descname"><span class="pre">attr5</span></span><em class="property"><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="pre">str</span></em><a class="headerlink" href="#awesome_project.exemple.ExampleClass.attr5" title="Permalink to this definition"></a></dt>
  <dd><p>Docstring <em>after</em> attribute</p>
  </dd></dl>

  <dl class="py property">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleClass.readonly_property">
  <em class="property"><span class="pre">property</span><span class="w"> </span></em><span class="sig-name descname"><span class="pre">readonly_property</span></span><a class="headerlink" href="#awesome_project.exemple.ExampleClass.readonly_property" title="Permalink to this definition"></a></dt>
  <dd><p>Properties should be documented in their getter method.</p>
  <dl class="field-list simple">
  <dt class="field-odd">Type</dt>
  <dd class="field-odd"><p>str</p>
  </dd>
  </dl>
  </dd></dl>

  <dl class="py property">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleClass.readwrite_property">
  <em class="property"><span class="pre">property</span><span class="w"> </span></em><span class="sig-name descname"><span class="pre">readwrite_property</span></span><a class="headerlink" href="#awesome_project.exemple.ExampleClass.readwrite_property" title="Permalink to this definition"></a></dt>
  <dd><p>Properties with both a getter and setter
  should only be documented in their getter method.</p>
  <p>If the setter method contains notable behavior, it should be
  mentioned here.</p>
  <dl class="field-list simple">
  <dt class="field-odd">Type</dt>
  <dd class="field-odd"><p><code class="xref py py-obj docutils literal notranslate"><span class="pre">list</span></code> of <code class="xref py py-obj docutils literal notranslate"><span class="pre">str</span></code></p>
  </dd>
  </dl>
  </dd></dl>

  <dl class="py method">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleClass.example_method">
  <span class="sig-name descname"><span class="pre">example_method</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">param1</span></span><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="n"><span class="pre">int</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">param2</span></span><span class="p"><span class="pre">:</span></span><span class="w"> </span><span class="n"><span class="pre">str</span></span></em><span class="sig-paren">)</span><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#awesome_project.exemple.ExampleClass.example_method" title="Permalink to this definition"></a></dt>
  <dd><p>Class methods are similar to regular functions.</p>
  <div class="admonition note">
  <p class="admonition-title">Note</p>
  <p>Do not include the <cite>self</cite> parameter in the <code class="docutils literal notranslate"><span class="pre">Args</span></code> section.</p>
  </div>
  <dl class="field-list simple">
  <dt class="field-odd">Parameters</dt>
  <dd class="field-odd"><ul class="simple">
  <li><p><strong>param1</strong> (<em>int</em>) – The first parameter.</p></li>
  <li><p><strong>param2</strong> (<em>str</em>) – The second parameter.</p></li>
  </ul>
  </dd>
  <dt class="field-even">Returns</dt>
  <dd class="field-even"><p>True if successful, False otherwise.</p>
  </dd>
  </dl>
  </dd></dl>

  <dl class="py method">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleClass.__special__">
  <span class="sig-name descname"><span class="pre">__special__</span></span><span class="sig-paren">(</span><span class="sig-paren">)</span><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#awesome_project.exemple.ExampleClass.__special__" title="Permalink to this definition"></a></dt>
  <dd><p>By default special members with docstrings are included.</p>
  <p>Special members are any methods or attributes that start with and
  end with a double underscore.</p>
  <p>Do not add a docstring but a simple comment if you don’t want it to appear</p>
  </dd></dl>

  <dl class="py method">
  <dt class="sig sig-object py" id="awesome_project.exemple.ExampleClass._private">
  <span class="sig-name descname"><span class="pre">_private</span></span><span class="sig-paren">(</span><span class="sig-paren">)</span><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#awesome_project.exemple.ExampleClass._private" title="Permalink to this definition"></a></dt>
  <dd><p>By default private members are included.</p>
  <p>Private members are any methods or attributes that start with an
  underscore and are <em>not</em> special.</p>
  <p>Do not add a docstring but a simple comment if you don’t want it to appear</p>
  </dd></dl>

  </dd></dl>

  </section>
