# Documentation

All you need to know to contribute to the documentation in this project.

## Sphinx 101

### Generating the documentation

This project is documented using Sphinx. To work properly, Sphinx should be used in a virtual environnement where the project has been installed through `pip -e '.[docs]'` or `pip -e '.[dev]'`.

Sphinx is a documentation generator that uses files in 'docs/source' to build a proper documentation in 'docs/build' (cf `make docs` to create an html version of the documentation).

 'docs/build' is ignored by git and can be remove without risk (cf `make clean-docs`).

### Writing documentation sources

Sphinx is configured to accept reStructuredText files (with .rst extension) and Markdown files (with either .txt or .md extension).

If you're new to these language, you might find some resources here :

- [Markdown : Standard Markdown](https://www.markdownguide.org/cheat-sheet/)
- [Markdown : MyST extension (used in Sphinx)](https://myst-parser.readthedocs.io/en/latest/syntax/reference.html)
- [reStructuredText : Getting started](https://sphinx-tutorial.readthedocs.io/step-1/)
- [reStructuredText : Cheatsheet](https://sphinx-tutorial.readthedocs.io/cheatsheet/#sphinx-cheat-sheet)
- [yet another guide](https://docs.readthedocs.io/en/stable/guides/authors.html)

Markdown is a bit simpler than reStructuredText, but reStructuredText is a bit more adapted to writing technical documentation (Markdown is more suited for simple web pages).

So a rule of thumbs should be that for description pages or tutorials, Markdown should be used; especially if non-technical team members may contribute but for technical documentation, one should choose reStructuredText.

One of the problem of native markdown is that it lacks textbox features like warnings or notes. This is solve by MyST which allow for special code-block names (like {note}, {error}, {warning} or {important}) :

```{note} Note title
This is a note
```

```{error}
This is an error
```

```{warning}
This is a warning
```

```{important}
This is important
```

### Index.rst

The file index.rst is the welcome page of the documentation. It is in reStructured form and is mainly composed of ..toctree:: directive.

toctree stands for "Table Of Content Tree" and is used with three principal options :

- :maxdepth: define the level ot titles and subtitles of the following files to show.
- :caption: give a tile to the toctree
- :hidden: if used, the toctree is not shown on the welcome page but still present in the side bar

### api-docs

Source contains a directory named api-docs which is filled automatically when `make docs` is run.

This part of the documentation is automatically generated from the doc string of the code in the `src` directory. This is thus a good reason to have a well documented codebase.

Sphinx accepts Numpy and Google docstring formats. It can also recognize type hint from [PEP 484](https://peps.python.org/pep-0484/) and [PEP 526](https://peps.python.org/pep-0526/).

```{warning}
To generate the doc, Sphinx source all the code base. So, the code should not have any side effects.  
For a python script that need to be run directly, it should be protected by a `if __name__ == "__main__":` directive
```

Finally, Google format should be preferred over Numpy as it is more compact vertically and easier to read (except for especially long docstrings).

The following links point to the code of a module named example of a virtual project named `awesome_project` and the result once transformed by Sphinx. This should serve as an example on how to write docstrings in this project.

```{toctree}
google_docstring_example_code
google_docstring_example_doc
```

### Testing Documentation

The make command ``make clean-docs`` run a check-link test (verify that all links are alive) and a coverage test (verify that all the python code base is documented).

The results appear in the terminal but also in the files `docs/build/output.txt` and `docs/build/python.txt` respectively.

Moreover, Sphinx is set to error on warning and to raise a warning if todo blocks are found. So the documentation might be build only if everything is ok.

## Advanced features

### Add math block

To add a mathematic formula :

- in a Markdown file use :
    - \$ my formula \$ for inline formula
    - \$\$ my formula \$\$ for block formula
- in a reStructuredText file use :
    - :math:\` my formula \` for inline formula
    - .. math:: (+ newline + indent) my formula (newline) for block formula

Inside the math markers, one can use amsmath standard syntax (as in latex).

It allows one to write anything from :

$$ a^2 + b^2 = c^2 $$

to :

$$ H_\mathbf{k}=\frac{1}{2m}(-i\hbar\mathbf{\nabla}+e\mathbf{A}(\mathbf{r})+\hbar \mathbf{k})^2+U(\mathbf{r}) $$

or even :

$$
    \begin{eqnarray}
    P^I_{k_y}&=&\frac{1}{2\pi}\int_{0}^{\pi}dk_x(A^{I}_{k_y}(k_x)+A^{I}_{k_y}(-k_x))\\
    &=&\frac{1}{2\pi}\int_{0}^{\pi}dk_x(A^{I}_{k_y}(k_x)+A^{II}_{-k_y}(k_x) +\nabla_{k_x}\chi_{k_x,-k_y}).
    \frac{1}{2\pi} \sum_\alpha (\chi_{\alpha,\pi,\Gamma_b}-\chi_{\alpha,0,\Gamma_b})
    \end{eqnarray}
$$

A "short" math guide for Latex is available [here](https://ctan.tetaneutral.net/info/short-math-guide/short-math-guide.pdf). It present a lot aof symbols available in amsmath, which is the math libraries used here.

### Link to other project documentation

intersphinx is enabled in this project and allow to link to other projects documentation.

If a project is linked and a reference is made to an object that do not belong to the current project, intersphinx will go and look in the referenced project. For example it is possible to reference the string class from python like this :  {py:class}`str`

To achieve this use :

- in Markdown : \{py:class\}\`str\`
- in reStructuredText : :py:class:\`str\`

By default, only the python3 built-in library is referenced, but one can add other projects by modifying the `intersphinx_mapping` variable in the conf.py file.

In `intersphinx_mapping`, the documentation of python3 is stored under the key `python`. This allow to reference complete documentation pages like this :

{doc}`python:tutorial/index`

To achieve this use :

- in Markdown : \{doc\}\`python:tutorial/index\`
- in reStructuredText : :doc:\`python:tutorial/index\`

Once again, the `intersphinx_mapping` variable in conf.py might be modified to add more documentation to whatever key.

Find more on [Sphinx page](https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html) and [readthedocs page](https://docs.readthedocs.io/en/stable/guides/intersphinx.html).

### Doc test

Sphinx doctest is enabled on this project. This enable one to test code snippets that are present in the doc to verify that they are still up to date.

To do that, you may write setup code block and/or a test code block and assign those block to a group. Then, when building the documentation, Sphinx will execute the setup blocks if there are any, and then the test blocks for each group.

You can use the following key-words as directive :

- **testsetup** : A setup code block. This code is not shown in the output for other builders, but executed before the doctests of the group(s) it belongs to.
- **testcleanup** : A cleanup code block. This code is not shown in the output for other builders, but executed after the doctests of the group(s) it belongs to.
- **testcode** : A code block for a code-output-style test.
- **testoutput** : The corresponding output, or the exception message, for the last testcode block.
- **doctest** : A code block for a code-output-style test, with inputs and outputs in the same block.

Then, while running `make test-docs`, if an input and the given output do not correspond, a warning is raise.

Here is an example (check the source file to see how it was done) :

The setup block is hidden (it imports math for all groups)

```{testsetup} *
import math
```

A doctest block

```{doctest} example-group1
>>> math.sqrt(4)
2.0
```

A test block in another group

```{testcode} example-group2
print(math.sqrt(9))
```

and the corresponding test block

```{testoutput} example-group2
3.0
```

```{note}
Remember that to use a directive you have to write : 

- in Markdown :  
\`\`\`{directive-name} argument  
The content of your block.  
\`\`\`
- in reStructuredText :    
.. directive-name:: argument  
&nbsp; &nbsp; The content of your block.  

Note that in this case, argument should be replaced by a group name.
```

Some more tips :

- if a * is used as group name for a setup block, it will be used for in all group.
- it is possible to run the test conditionally. See documentation on sphinx.  
- an output block might be hidden (adding :hide: below the block directive)
- there is no consistency check at build time, so the documentation might be build even if doctest fails.

Find more on [Sphinx page](https://www.sphinx-doc.org/en/master/usage/extensions/doctest.html)
