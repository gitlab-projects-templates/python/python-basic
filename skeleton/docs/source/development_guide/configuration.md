# Configuration

If you need to use configuration files in this python project you may use the
[tools.config module](../api-docs/tools.config.rst).  
You will find here a few hints on how to use it.

## Adding configuration files

By default, the config.Configs class looks for configuration files in config_files.default and config_files.local.

Configuration files in config_files.default are meant to be shared on git and added to the python package. Except for
credentials, that should be provided by another way, it should contains an entry for all configuration needed by the
app.

Configuration files in config_files.local are NOT shared on git and NOT added to the python package. They are meant to
overwrite configuration in default to match local environnement. They may contains credentials entries.

Note that config_files.local and config_files.default are packages (you need a \_\_init\_\_.py file inside) while
config_files is an implicit namespace package. This mean that you can put another config_files directory in any place
visible by PYTHONPATH and **without** an \_\_init\_\_.py inside to make any sub-package inside visible by the
tools.config module. See more on implicit namespace package in the [PEP 420](https://peps.python.org/pep-0420/).

```{warning}
All configuration files whose name contain the string 'credential' are ignored by git.
This is a safety measure, but this is not fool proof... 
Be careful not to put any credentials in shared configuration files.
```

## Using configuration value in the code

To load a configuration file in your code you should do:

```{doctest} import_config
>>> from tools import config
>>> configs = config.Configs()
```

Then, assuming you have a logger.ini and parameter.json in your configuration package:

```python
configs.load("logger")
configs.load("parameter", conf_type="json")
```

And finally access the values inside with :

```python
param1 = configs.parameter['param1']
```

For more information, you may look directly at the [api-doc](../api-docs/tools.config.rst)

## The different file formats

The accepted file format are .ini, .json and .toml.

- .ini extension :
    - has been used extensively in python
    - is parsed by the built-in ConfigParser
    - do not accept types (everything is a string)
    - accepts comments
- .json extension :
    - is used extensively in the web universe
    - is parsed by the built-in json
    - is typed (but do not know datetime type)
    - doesn't accept comments
- .toml extension :
    - is the new *de facto* new standard in python
    - is parsed by the built-in tomllib in python 3.11+ (and by the external package tomli for previous versions of python)
    - is typed, including datetime
    - accepts comments
