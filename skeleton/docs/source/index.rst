.. {{config.project_name}} documentation master file, created by
   sphinx-quickstart on Sat Oct 29 20:47:22 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to {{config.project_name}}'s documentation!
===================================================

{{config.project_description}}

Contents
--------

.. toctree::
   :maxdepth: 2
   :caption: First steps

   ReadMe <README>

.. toctree::
   :maxdepth: 2
   :caption: API Documentation

   {{config.python_project_name}} <api-docs/{{config.python_dir}}>
   tools <api-docs/tools>

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Miscellaneous

   Changelog <CHANGELOG>

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Development Guide

   development_guide/documentation
   development_guide/configuration

.. toctree::
   :hidden:
   :caption: Project Links

   Gitlab <https://{{config.gitlab_server}}/{{config.gitlab_namespace}}/{{config.gitlab_slug}}>
   Bug Tracker <https://{{config.gitlab_server}}/{{config.gitlab_namespace}}/{{config.gitlab_slug}}/-/issues>

:ref:`genindex`
===============
