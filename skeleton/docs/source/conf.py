# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "{{config.project_name}}"
project_copyright = "{{config.year}}, {{config.project_authors}}"
author = "{{config.project_authors}}"
version = "{{config.project_version}}"
release = "{{config.project_version}}"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

needs_sphinx = "3.5"
numfig = True
pygments_style = "sphinx"

extensions = [
    "myst_parser",
    "sphinx.ext.intersphinx",
    "sphinx.ext.coverage",
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.duration",
    "sphinx.ext.mathjax",
    "sphinx.ext.napoleon",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
]

templates_path = ["_templates"]

source_suffix = {
    ".rst": "restructuredtext",
    ".txt": "markdown",
    ".md": "markdown",
}

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]

# -- Options for intersphinx -------------------------------------------------

intersphinx_mapping = {"python": ("https://docs.python.org/3.9/", None)}

# -- Options for todo --------------------------------------------------------

todo_emit_warnings = True

# -- Options for MyST --------------------------------------------------------

myst_heading_anchors = 5
myst_enable_extensions = ["dollarmath"]

# -- Options for autodoc -----------------------------------------------------

autodoc_default_options = {
    "autoclass_content": "class",
    "member-order": "bysource",
    "members": True,
    "show-inheritance": True,
}
autodoc_typehints = "signature"

# -- Options for napoleon ----------------------------------------------------

napoleon_google_docstring = True
napoleon_numpy_docstring = True
napoleon_include_init_with_doc = True
napoleon_include_private_with_doc = True
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = True
napoleon_use_admonition_for_notes = True
napoleon_use_admonition_for_references = True
napoleon_use_ivar = False
napoleon_use_param = True
napoleon_use_rtype = True
napoleon_use_keyword = True
napoleon_preprocess_types = False
napoleon_type_aliases = None
napoleon_attr_annotations = True
