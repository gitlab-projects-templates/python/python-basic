"""Tool to get configuration"""

from __future__ import annotations

import configparser
import json
import os
import sys
from collections.abc import Iterator
from dataclasses import dataclass
from importlib import resources
from typing import Any, Callable

try:
    import tomllib  # type: ignore
except ImportError:
    import tomli as tomllib  # type: ignore


class ConfError(ValueError):
    """Error in configuration definition"""

    pass


@dataclass
class Configs:
    """Class which loads and stores configuration value from files or environnement variable

    This class looks for configuration files in the `conf_package` package.
    `conf_package` should be organized in sub-packages that holds the different versions
    of the configuration (local VS default configuration files for example).
    This class looks for configuration in `env` and `default_env` (`env` takes precedence).

    Example::

        To use this class with default values, you should organize your config files as follow :
            .
            `-- config_files
                |-- __init__.py
                |-- default
                |   |-- __init__.py
                |   |-- logger.ini          ----> in version control
                |   `-- parameters.json     ----> in version control
                `-- local
                    |-- __init__.py
                    |-- credentials.ini     ----> NOT in version control
                    `-- logger.ini          ----> NOT in version control

        And use :
        ```python
        config = Configs()              # initialize a config object
        config.load("logger", "ini")    # load configs from logger.ini in config.logger
                                        # logger.ini in local/ takes precedence over default/
        config.load("credentials", "ini")   # load credentials.ini in config.credentials
        config.load("parameters", "json")   # load parameters.json in config.parameters
        ```

    You may add a `test` folder on the same level as default and local and use it to load a default
    configuration during test rather than mocking the configuration object.

    Args:
        conf_package: package in which to look for config files. Default to 'config_files'
        env: subpackage of `conf_package` where to look for configuration. Default to 'local'
        default_env: subpackage of `conf_package` where to look for default configuration. Default
            to 'default'
        default_conf_type: default extension to use for configuration files. Default to 'ini'.
    """

    conf_package: str = "config_files"
    env: str = "local"
    default_env: str = "default"
    default_conf_type: str = "toml"

    def load(self, name: str, conf_type: str | None = None, attr_name: str | None = None) -> None:
        """Load configuration from file or environnement variable and store it in dict form in an
        attribute named `attr_name`

        Args:
            name: name of the file (without extension) or environnement variable to load
            type: use file extension for configuration files or 'env' for environnement variable.
                type must be one of ['env', 'ini', 'json', 'toml']
            attr_name: attribute to create to store configuration. Default to value of `name`

        """
        load_config_dict: dict[str, Callable[..., dict[str, Any]]] = {
            "ini": self._read_conf_from_ini_resource,
            "json": self._read_conf_from_json_resource,
            "toml": self._read_conf_from_toml_resource,
        }

        if attr_name is None:
            attr_name = name
        if conf_type is None:
            conf_type = self.default_conf_type

        if attr_name in dir(self):
            raise ValueError(f"attr_name value ({attr_name}) is protected or already used")

        if conf_type not in [*load_config_dict.keys(), "env"]:
            raise ValueError(
                f"type should take value in {load_config_dict.keys} but found {conf_type}"
            )

        conf_dict: dict[str, Any]
        if conf_type == "env":
            conf_dict = self._read_conf_from_environment(name)
        else:
            conf_dict = self._loop_in_conf_package(
                ".".join([name, conf_type]), load_config_dict[conf_type]
            )

        self.__setattr__(attr_name, conf_dict)

    def _loop_in_conf_package(
        self, file_name: str, reader: Callable[..., dict[str, Any]]
    ) -> dict[str, Any]:
        """Given a file_name and a reader, loop over `self.env` and `self.default_env`
        in `conf_package` to find configuration files named `filed_named` and parse
        them with `reader`.

        Configuration found in `self.env` takes precedence over configuration found in
        `self.default_env`.

        Args:
            file_name: name of the file to load
            reader: a reader that return a dict from an importlib resource
        """
        env_package: str = ".".join([self.conf_package, self.env])
        file_not_found_in_env: bool = False
        file_not_found_in_default_env: bool = False
        try:
            conf_dict_env: dict[str, Any] = reader(env_package, file_name)
        except FileNotFoundError:
            file_not_found_in_env = True

        default_env_package: str = ".".join([self.conf_package, self.default_env])
        try:
            conf_dict_default_env: dict[str, Any] = reader(default_env_package, file_name)
        except FileNotFoundError:
            file_not_found_in_default_env = True

        if file_not_found_in_env and file_not_found_in_default_env:
            raise FileNotFoundError(f"Couldn't find {file_name} in any env folder")

        return dict(_merge_conf_dicts(conf_dict_default_env, conf_dict_env))

    @staticmethod
    def _read_conf_from_environment(environment_variable: str) -> dict[str, str]:
        """config reader for environment variable"""

        environment_variable_value: str | None = os.getenv(environment_variable)
        if not environment_variable_value:
            raise ConfError(
                f"Environnement variable {environment_variable} do not exist or is empty"
            )
        return {environment_variable: environment_variable_value}

    @staticmethod
    def _read_conf_from_ini_resource(env_package: str, file_name: str) -> dict[str, Any]:
        """config reader for .ini files"""
        config_obj: configparser.ConfigParser = configparser.ConfigParser()
        try:
            if sys.version_info < (3, 9):
                config_obj.read_string(
                    resources.read_text(env_package, file_name, encoding="utf-8")
                )
            else:
                config_obj.read_string(
                    resources.files(env_package).joinpath(file_name).read_text(encoding="utf-8")
                )
        except configparser.Error:
            raise ConfError(f"Couldn't parse ini file {file_name} in {env_package})")

        return {s: dict(config_obj.items(s)) for s in config_obj.sections()}

    @staticmethod
    def _read_conf_from_toml_resource(env_package: str, file_name: str) -> dict[str, Any]:
        """config reader for .toml files"""
        try:
            config_dict: dict[str, Any]
            if sys.version_info < (3, 9):
                config_dict = tomllib.loads(
                    resources.read_text(env_package, file_name, encoding="utf-8")
                )
            else:
                config_dict = tomllib.loads(
                    resources.files(env_package).joinpath(file_name).read_text(encoding="utf-8")
                )
        except tomllib.TOMLDecodeError:
            raise ConfError(f"Couldn't parse TOML file {file_name} in {env_package})")
        return config_dict

    @staticmethod
    def _read_conf_from_json_resource(env_package: str, file_name: str) -> dict[str, Any]:
        """config reader for .json files"""
        try:
            config_dict: dict[str, Any]
            if sys.version_info < (3, 9):
                config_dict = json.loads(
                    resources.read_text(env_package, file_name, encoding="utf-8")
                )
            else:
                config_dict = json.loads(
                    resources.files(env_package).joinpath(file_name).read_text(encoding="utf-8")
                )
        except json.JSONDecodeError:
            raise ConfError(f"Couldn't parse json file {file_name} in {env_package})")

        if not isinstance(config_dict, dict):
            raise ConfError(
                f"""First level structure of json configuration file should be an Object (dict)
                (in {file_name} in {env_package})"""
            )
        return config_dict


def _merge_conf_dicts(dict1: dict[str, Any], dict2: dict[str, Any]) -> Iterator[tuple[str, Any]]:
    """Merge dict2 into dict1, value in dict2 overwrites values in dict1"""
    for k in set(dict1.keys()).union(dict2.keys()):
        if k in dict1 and k in dict2:
            if isinstance(dict1[k], dict) and isinstance(dict2[k], dict):
                yield (k, dict(_merge_conf_dicts(dict1[k], dict2[k])))
            else:
                yield (k, dict2[k])
        elif k in dict1:
            yield (k, dict1[k])
        else:
            yield (k, dict2[k])
