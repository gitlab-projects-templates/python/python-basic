# {{config.project_name}}

[![pipeline status](https://{{config.gitlab_server}}/{{config.gitlab_namespace}}/{{config.gitlab_slug}}/badges/main/pipeline.svg?ignore_skipped=true)](https://{{config.gitlab_server}}/{{config.gitlab_namespace}}/{{config.gitlab_slug}}/-/commits/main)
[![coverage report](https://{{config.gitlab_server}}/{{config.gitlab_namespace}}/{{config.gitlab_slug}}/badges/main/coverage.svg)](https://{{config.gitlab_server}}/{{config.gitlab_namespace}}/{{config.gitlab_slug}}/-/commits/main)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-%23FE5196?logo=conventionalcommits&logoColor=white)](https://www.conventionalcommits.org)
[![img](https://img.shields.io/badge/semver-2.0.0-green)](https://semver.org/)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)

{{config.project_description}}

## Getting Started

### Requirements

- Python 3.{{config.python3_minimum_required_minor}}+
- pip 21.2+

[back to top](#{{config.project_name_anchor}})

### Installation

To install this project simply cd to the place where you want to install it, clone this repository and run pip install :

1. cd to the desired emplacement

    ```sh
    cd </path/to/project/directory>
    ```

1. clone this repository

    ```sh
    git clone "https://{{config.gitlab_server}}/{{config.gitlab_namespace}}/{{config.gitlab_slug}}.git"
    ```

1. cd to project directory and run pip (**preferably in a virtual environnement**)

    ```sh
    cd "{{config.gitlab_slug}}"
    pip install .
    ```

    Note : for a development mode install use the following code instead :

    ```sh
    cd "{{config.gitlab_slug}}"
    pip install -e '.[dev]'
    ```

[back to top](#{{config.project_name_anchor}})

## License

{{config.project_name}}  
Copyright (C) {{config.year}}  {{config.project_authors}}  
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

[back to top](#{{config.project_name_anchor}})
