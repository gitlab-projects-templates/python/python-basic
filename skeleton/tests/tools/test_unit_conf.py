from __future__ import annotations

import os
from typing import Any

import pytest

from tools import config


def test_init_configs_with_default() -> None:
    config_obj: config.Configs = config.Configs()
    assert config_obj.conf_package == "config_files"
    assert config_obj.env == "local"
    assert config_obj.default_env == "default"
    assert config_obj.default_conf_type == "toml"


def test_init_config() -> None:
    config_obj: config.Configs = config.Configs(
        conf_package="conf_package",
        env="env",
        default_env="default_env",
        default_conf_type="default_conf_type",
    )
    assert config_obj.conf_package == "conf_package"
    assert config_obj.env == "env"
    assert config_obj.default_env == "default_env"
    assert config_obj.default_conf_type == "default_conf_type"


@pytest.fixture
def config_default() -> config.Configs:
    config_obj = config.Configs(conf_package="test_config_data")
    return config_obj


def test_load_attr_protection(config_default: config.Configs) -> None:
    for protected_attr in [
        "__annotations__",
        "__class__",
        "__dataclass_fields__",
        "__dataclass_params__",
        "__delattr__",
        "__dict__",
        "__dir__",
        "__doc__",
        "__eq__",
        "__format__",
        "__ge__",
        "__getattribute__",
        "__gt__",
        "__hash__",
        "__init__",
        "__init_subclass__",
        "__le__",
        "__lt__",
        "__module__",
        "__ne__",
        "__new__",
        "__reduce__",
        "__reduce_ex__",
        "__repr__",
        "__setattr__",
        # "__sizeof__",
        "__str__",
        "__subclasshook__",
        "__weakref__",
        "_loop_in_conf_package",
        "_read_conf_from_environment",
        "_read_conf_from_ini_resource",
        "_read_conf_from_json_resource",
        "_read_conf_from_toml_resource",
        "conf_package",
        "default_conf_type",
        "default_env",
        "env",
        "load",
    ]:
        with pytest.raises(ValueError):
            config_default.load("test_ini", attr_name=protected_attr)


def test_load_conf_type_protection(config_default: config.Configs) -> None:
    with pytest.raises(ValueError):
        config_default.load("test_ini", conf_type="wrong_conf_type")


def test_load_env(config_default: config.Configs) -> None:
    path_value: str | None = os.getenv("PATH")
    config_default.load("PATH", conf_type="env", attr_name="path")
    assert config_default.path["PATH"] == path_value  # type: ignore


def test_load_empty_env(config_default: config.Configs) -> None:
    with pytest.raises(config.ConfError):
        config_default.load("uieazcijezahui", conf_type="env")


def test_merge_conf_dicts() -> None:
    merged_dict: dict[str, Any] = {
        "A": {"A": 1, "B": 1},
        "B": [1, 2, 3],
        "C": {"A": {"A": "A"}},
        "D": 1,
        "E": 1,
        "F": {1, 2},
    }
    dict_1: dict[str, Any] = {
        "A": {"A": 1, "B": 2},
        "B": [1, 4],
        "C": {"A": {"A": "B"}},
        "E": 1,
        "F": "Hello",
    }
    dict_2: dict[str, Any] = {
        "A": {"A": 1, "B": 1},
        "B": [1, 2, 3],
        "C": {"A": {"A": "A"}},
        "D": 1,
        "F": {1, 2},
    }
    assert merged_dict == dict(config._merge_conf_dicts(dict_1, dict_2))
