from typing import Any

import pytest

from tools import config


@pytest.fixture
def config_default() -> config.Configs:
    config_obj = config.Configs(conf_package="test_config_data")
    config_obj.load("test_toml")
    return config_obj


@pytest.fixture
def config_with_dev_env() -> config.Configs:
    config_obj = config.Configs(conf_package="test_config_data", env="dev")
    config_obj.load("test_toml")
    return config_obj


@pytest.fixture
def config_inverse_default_and_local() -> config.Configs:
    config_obj = config.Configs(
        conf_package="test_config_data", env="default", default_env="local"
    )
    config_obj.load("test_toml")
    return config_obj


def test_env_overwrites_default_env(config_default: config.Configs) -> None:
    toml_section1: dict[str, str] = config_default.test_toml["Section 1"]  # type: ignore
    assert toml_section1["key_everywhere"] == "value_in_local"
    assert toml_section1["key_in_default"] == "value_in_default"
    assert toml_section1["key_in_local"] == "value_in_local"


def test_env_parameter(config_with_dev_env: config.Configs) -> None:
    toml_section1: dict[str, str] = config_with_dev_env.test_toml["Section 1"]  # type: ignore
    assert toml_section1["key_everywhere"] == "value_in_dev"
    assert toml_section1["key_in_default"] == "value_in_default"
    assert toml_section1["key_in_dev"] == "value_in_dev"


def test_default_env_parameter(config_inverse_default_and_local: config.Configs) -> None:
    toml_section1: dict[str, str] = config_inverse_default_and_local.test_toml[  # type: ignore
        "Section 1"
    ]
    assert toml_section1["key_everywhere"] == "value_in_default"
    assert toml_section1["key_in_default"] == "value_in_default"
    assert toml_section1["key_in_local"] == "value_in_local"


def test_raise_error_if_file_not_found_in_default_env(config_default: config.Configs) -> None:
    with pytest.raises(FileNotFoundError):
        config_default.load("inexistant_file", conf_type="json")


def test_load_ini(config_default: config.Configs) -> None:
    config_default.load("test_ini", conf_type="ini")
    ini_section2: dict[str, str] = config_default.test_ini["Section 2"]  # type: ignore
    assert ini_section2["key with space"] == "value with space"
    assert ini_section2["integer"] == "3"
    assert ini_section2["float"] == "3.5"
    assert ini_section2["boolean"] == "True"
    assert ini_section2["multiline"] == "this is a\nmultiline value"
    assert ini_section2["semicolon"] == "separator"
    with pytest.raises(config.ConfError):
        config_default.load("test_ini_wrong", conf_type="ini")


def test_load_json(config_default: config.Configs) -> None:
    config_default.load("test_json", conf_type="json")
    json_section1: dict[str, Any] = config_default.test_json["Section 1"]  # type: ignore
    assert json_section1["key_everywhere"] == "value_in_local"
    assert json_section1["key_in_default"] == "value_in_default"
    assert json_section1["key_in_local"] == "value_in_local"
    json_section2: dict[str, Any] = config_default.test_json["Section 2"]  # type: ignore
    assert json_section2["key with space"] == "value with space"
    assert json_section2["integer"] == 3
    assert json_section2["float"] == 3.5
    assert json_section2["boolean"] is True
    assert json_section2["Null"] is None
    assert json_section2["multiline"] == "this is a\nmultiline value"
    assert config_default.test_json["outside_key"] == "outside_value"  # type: ignore
    with pytest.raises(config.ConfError):
        config_default.load("test_json_wrong", conf_type="json")
    with pytest.raises(config.ConfError):
        config_default.load("test_json_not_dict", conf_type="json")


def test_load_toml(config_default: config.Configs) -> None:
    toml_section1: dict[str, Any] = config_default.test_toml["Section 1"]  # type: ignore
    assert toml_section1["key_everywhere"] == "value_in_local"
    assert toml_section1["key_in_default"] == "value_in_default"
    assert toml_section1["key_in_local"] == "value_in_local"
    toml_section2: dict[str, Any] = config_default.test_toml["Section 2"]  # type: ignore
    assert toml_section2["key with space"] == "value with space"
    assert toml_section2["integer"] == 3
    assert toml_section2["float"] == 3.5
    assert toml_section2["boolean"] is True
    assert toml_section2["multiline"] == "this is a\nmultiline value"
    assert config_default.test_toml["outside_key"] == "outside_value"  # type: ignore
    assert config_default.test_toml["sous"]["section1"]["key"] == "value"  # type: ignore
    assert config_default.test_toml["sous"]["section2"]["key"] == "value"  # type: ignore
    with pytest.raises(config.ConfError):
        config_default.load("test_toml_wrong", conf_type="toml")
