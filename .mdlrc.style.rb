all
exclude_rule 'MD013'

rule 'MD007', :indent => 4
rule 'MD009', :br_spaces => 2
rule 'MD024', :allow_different_nesting => true